package richtercloud.jee.slf4j.logging.jar;

import java.util.Random;
import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author martin
 */
@Stateless
public class DefaultMyController implements MyController {
    private final static Logger LOGGER = LoggerFactory.getLogger(DefaultMyController.class);
    private final static Random RANDOM = new Random();

    @Override
    public MyEntity lotsOfEmpireStuff() {
        MyEntity myEntity = new MyEntity(1L,
                String.valueOf(RANDOM.nextInt()));
        LOGGER.trace(String.format("look busy: %s",
                myEntity));
        return myEntity;
    }
}
