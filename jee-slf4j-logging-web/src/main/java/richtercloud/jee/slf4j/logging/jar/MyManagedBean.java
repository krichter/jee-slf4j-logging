package richtercloud.jee.slf4j.logging.jar;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author richter
 */
@Named
@ViewScoped
public class MyManagedBean implements Serializable {
    private final static Logger LOGGER = LoggerFactory.getLogger(MyManagedBean.class);
    private static final long serialVersionUID = 1L;
    @EJB
    private MyController myController;

    public MyManagedBean() {
    }

    @PostConstruct
    public void init() {
        LOGGER.trace("MyManagedBean initialized");
    }

    public String initMyEntity() {
        LOGGER.trace("invoking initMyEntity");
        MyEntity myEntity = myController.lotsOfEmpireStuff();
        return myEntity.getMyProperty();
    }
}
