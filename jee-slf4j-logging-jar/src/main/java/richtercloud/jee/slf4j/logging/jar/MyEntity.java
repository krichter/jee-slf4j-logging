package richtercloud.jee.slf4j.logging.jar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author richter
 */
public class MyEntity {
    private final static Logger LOGGER = LoggerFactory.getLogger(MyEntity.class);
    private Long id;
    private String myProperty;

    public MyEntity() {
    }

    public MyEntity(Long id, String myProperty) {
        this.id = id;
        this.myProperty = myProperty;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        LOGGER.trace(String.format("set id to %d",
                id));
        this.id = id;
    }

    public String getMyProperty() {
        return myProperty;
    }

    public void setMyProperty(String myProperty) {
        this.myProperty = myProperty;
    }
}
