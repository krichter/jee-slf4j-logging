package richtercloud.jee.slf4j.logging.jar;

import javax.ejb.Local;

/**
 *
 * @author richter
 */
@Local
public interface MyController {

    MyEntity lotsOfEmpireStuff();
}
